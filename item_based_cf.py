import argparse
import pandas as pd
import numpy as np
from scipy.spatial.distance import cosine
import pickle

# data_path : raw data(csv file)
# output_dir : directory to save output
# global_index_path : global_index
# item_list : list containing item id for getting a recommendation item
# n : number of top output(default = 10)


def main(data_path, output_dir, global_index_path, item_id_list, n=10):

    # load data
    data = pd.read_csv(data_path)

    # remove user_id column
    # hard coding
    data = data.drop('id', 1)

    # Create dataframe, item by item
    item_by_item_data = pd.DataFrame(index=data.columns, columns=item_id_list)
    row_size = len(item_by_item_data.index)

    # fill in those empty spaces with cosine similarities
    # if same index-to-index operation - nan
    for item_index in item_id_list:
        for row_index in range(0, row_size):
            # Remove cosine similarities of same item_id
            if item_index == data.columns.values[row_index]:
                item_by_item_data.ix[row_index, item_index] = np.nan
            else:
                item_by_item_data.ix[row_index, item_index] = round(1-cosine(data.ix[:, row_index], data.ix[:, item_index]), 3)

    # global stock index check
    global_index = pd.read_csv(global_index_path, header=None)
    list_global_index = global_index.ix[:, 0].values.tolist()

    # filter only global stock in result
    filter_global = item_by_item_data.filter(items=list_global_index, axis=0)

    # Save dictionary output to pkl file
    dic = {}

    for item_id in item_id_list:
        recommendation_items = filter_global[item_id]
        # set number of top
        sort_recommendation_items = recommendation_items.sort_values(ascending=False)[:n]
        # input item_id and recommendation items in dictionary
        dic.setdefault(item_id, sort_recommendation_items.index.tolist())

    output_path = ('%s/recommendation_output.pkl' % output_dir)
    dump_loc = open(output_path, 'wb')
    pickle.dump(dic, dump_loc)
    dump_loc.close

    '''''''''
    # sort result
    sort_result = filter_result.sort_values(by=item_id, ascending=False)

    # set number of top
    top_n_result = sort_result.ix[:n, :]

    # modify column name
    top_n_result.index.name = 'recommendation_item'
    top_n_result.columns = ['score']

    # save
    output_path = ('%s/%s_recommendation_output.csv' % (output_dir, item_id))
    top_n_result.to_csv(output_path)
    '''''''''


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Recommendation engine test')
    parser.add_argument('data_path', type=str)
    parser.add_argument('output_dir', type=str)
    parser.add_argument('global_index_path', type=str)
    parser.add_argument('--item_id_list', '-l', nargs='+', type=str)
    parser.add_argument('--n','-n', type=int, default=10)

    args = parser.parse_args()
    main(args.data_path, args.output_dir, args.global_index_path, args.item_id_list, args.n)

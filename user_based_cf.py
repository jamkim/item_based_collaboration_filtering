import argparse
import pandas as pd
import numpy as np

# data_path : /home/knowru/Desktop/jaemin/development/recommendation/data/sample_data.csv
# output dir :/home/knowru/Desktop/jaemin/project2


def main(data_path, output_dir, user_id):

    # data load
    data = pd.read_csv(data_path, sep=',')

    # column names check
    column_names = data.columns.values

    user_column = column_names[0]
    item_column = column_names[1]
    category_column = column_names[2]
    rating_column = column_names[3]

    # total user count
    total_user_count = len(user_column)


    # similarity ; Check correlation coefficient by user
    correlation_result = pd.DataFrame(data.corr(method='pearson'))

    # Remove the correlation coefficient with oneself
    np.fill_diagonal(correlation_result.values, np.nan)

    user_corr_data = correlation_result.ix[:, user_id]
    sort_data = user_corr_data.sort_values(ascending=False)
    corr_top_n_output = sort_data[0:total_user_count]


    # 최종 결과
    output = []
    # 사용자별, item별 스코어
    score = 0
    score_dic = {}
    # 사용자별, item별 유사도
    similarity_dic = {}

    # 사용자 데이터 구분
    user_data = data.loc[data['user_id'] == user_id].reset_index()
    user_item = user_data.item_id.values

    # 데이터내에 item unique 출력
    total_item = data.item_id.unique()

    # 사용자가 사용하지 않은 item 출력
    user_not_use_item = list(set(total_item) - set(user_item))

    for opponent_id in corr_top_n_output.index:
        # 사용자와 상대방 유사도 확인
        similarity = corr_top_n_output[opponent_id]
        # 양수일 경우만 분석
        if similarity > 0:
            # 상대방 데이터 중 사용자가 사용하지 않은 item 구분
            opponent_data = data.loc[data['user_id'] == opponent_id].reset_index()
            opponent_use_item = set(user_not_use_item).intersection(opponent_data.item_id.values)

            for opponent_item in opponent_use_item:
                # 상대방의 rating 확인
                opponent_rating = opponent_data.loc[opponent_data['item_id'] == opponent_item].rating.values
                # 상대방과의 유사도와 rating을 곱하여 score로 출력
                score += similarity*opponent_rating[0]
                # print(opponent_id, opponent_item, score)

                score_dic.setdefault(opponent_item, 0)
                score_dic[opponent_item] += score

                similarity_dic.setdefault(opponent_item, 0)
                similarity_dic[opponent_item] += similarity

                score = 0

    for key in score_dic:
        # 평점 총합 / 유사도 총합 계산
        score_dic[key] = score_dic[key] / similarity_dic[key]
        output.append((user_id, key, score_dic[key]))

    output_dataframe = pd.DataFrame(output, columns=['user_id', 'item_id', 'score'])

    output_dataframe= output_dataframe.sort_values(by=['score'], ascending=False)

    output_path = ('%s/output.csv' % output_dir)
    output_dataframe.to_csv(output_path)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Recommendation Engine Test')
    parser.add_argument('data_path', type=str)
    parser.add_argument('output_dir', type=str)
    parser.add_argument('user_id', type=str)
    args = parser.parse_args()
    main(args.data_path, args.output_dir, args.user_id)

import pandas as pd
import numpy as np
import argparse

def main(data_path, output_dir):

    # data load
    data = pd.read_csv(data_path)

    # category - 0 : domestic , 1 : global
    global_index = data.query('category == 1')['item_id'].unique()

    # save output
    output_path = ("%s/global_index.csv" % output_dir)
    np.savetxt(output_path, global_index,
               delimiter=",", fmt='%s')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='create global index data')
    parser.add_argument('data_path', type=str)
    parser.add_argument('output_dir', type=str)
    args = parser.parse_args()
    main(args.data_path, args.output_dir)
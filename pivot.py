import pandas as pd
import argparse


# data_path : raw data(csv file)
# output_dir : directory to save output

def main(data_path, output_dir):

    data = pd.read_csv(data_path)

    column_names = data.columns.values

    user_column = column_names[0]
    item_column = column_names[1]
    satus_column = column_names[3]

    # pivot table : Check rating value by user / item
    result = data.pivot(index=user_column, columns=item_column, values=satus_column)

    result = result.fillna(0)

    output_path = ("%s/pivot_data.csv" % output_dir)
    result.to_csv(output_path)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='create pivot data')
    parser.add_argument('data_path', type=str)
    parser.add_argument('output_dir', type=str)
    args = parser.parse_args()
    main(args.data_path, args.output_dir)




